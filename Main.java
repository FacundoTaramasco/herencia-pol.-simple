
/**
 * Write a description of class Main here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Main{
    public static void main(String[] args){
        /*
        ClaseHija1 c1 = new ClaseHija1();
        ClaseHija2 c2 = new ClaseHija2();
        ClaseHija3 c3 = new ClaseHija3();
                
        c1.metodoPolimorfico();
        c2.metodoPolimorfico();
        c3.metodoPolimorfico();
        */

        ClasePadre[] vecp = new ClasePadre[3];
        
        vecp[0] = new ClaseHija1();
        vecp[1] = new ClaseHija2();
        vecp[2] = new ClaseHija3();
        
        /*for (int i = 0 ; i < 3 ; i++){
            vecp[i].metodoPolimorfico();
        }*/

        for (ClasePadre claseActual : vecp){
            claseActual.metodoPolimorfico();
        }
        
    }
}
